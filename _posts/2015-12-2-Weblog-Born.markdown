---
layout: page
title: 'Weblog Born'
date: 2015-12-2 13:05:39 +0330
version: 3.0.1
author: Alireza
categories: [News]
---
Hey, folks :tophat:

I had a similar weblog system around a year ago, but back then there were no many visitors, and I got busy with other stuff in life and one day, I found myself deleting the folder on my pc and then the repository on my source control account on bit bucket :boom:. At that time I thought the system I was using was not the best solution :persevere: and wanted to go for a real solution :eyes:!

You see, back then, and right now, I use a system called Jekyll, actually, if you check their website, you would see that the template I am using here is a modified and simpler copy of their template. So, what is jekyll ? It belongs to a category of script / software that creates static page websites. What do I mean by that ? Back in old days, before other web languages like php, asp and other come to life, html and css where the only ways to develop a web site. At those times, you had to sit in front of pc, and design each and every individual page, and you can just imagine that if you found that you had to add a new item in the main menu of the website, specially when you had 50 pages, then you had to sit and open each and every one of those pages and inject the menu into them. Later on these type of things became a bit easier by creating menues with java scripts. But still, the content of the page itself had to be designed by the user per page base !

That is until some languages like php and asp came to life and people through away html and css, they would only come back to these base stuff if the thing they had to do was not available in any other languages. Using php, some team foundations, developed very advance CMS (Content Management System) for establishing web sites easier and faster. Systems like wordpress and joomla are in that category. But, as time passed, more and more people started questioning the usage of these heavy systems. Those system would make the life of content publishers, specially weblog writers, easier. But at the same time, each one of them had their own bugs, their own security issues and few other things that I will talk about them later. After Ruby and Python languages came life, some people started using these for web related content, and before you know what was happening, a system was born, Jekyll. Jekyll was the first application in this category. The concept behind Jekyll is that the developer will write parsers, style sheets and many more stuff needed once and for all, then these systems, would part text files that are placed by user in special folders, read their content, part them and look for special tags and syntaxes and then generate HTML files for the user and the final product.

There are some things regarding Joomla and Word press that caused more people come to use Jekyll and similar solutions more and more :

1. When you want to use joomla or wordpress, you have to get a hosting plan from a provider, this causes money.
2. You have to learn how to maintain a mysql data base because those systems heavily use the data base
3. When you want to publish a post, you have to be connected to the internet while you are typing and editing
4. If you want to design your own template, the process of installing Apache, mysql and other stuff needed can make some people crazy
5. Designing a good looking web page is hard because beside HTML and css you need to learn php
6. Each one of those systems have their own learning curve. For wordpress it's no big issue but joomla can take quit some time to learn it
7. Each one of those system have their own security issues. Getting spams in your chat boxes, or comments are a very common thing in those systems
8. To have additional featuers in those systems, you have to download and install plugins, and the process can be complex some times. Also, that means more stuff to learn.

And the list continues on and on. For people like me who want to just post stuff as weblog, this is a big head ache, I had worked with both system and to have somthing as commonly used as code blocks and syntax highlighting in it, I had to go, search for plugins and intall them. And that would only be the start of night mare. Then when my local installation was ready (something that would take even a week), finally I had to spend money and find a hosting service, install the system on the hosting and upload my template, and that was when I was able to start blogging.

On the other hand I could use already available blogging web sites, but again, either they were commercial and cost moeny, or, they had a rigid prefabricated system that would not give me freedom for page designes.

For the same reason, many people started using Jekyll and later on other solutions with the same concept. It only took me 2 hours to change the Jekyll's website template to the thing you are seeing here right now. And the most of that time was wasted on try and error because I have absolutely 0 amount of knowledge regarding HTML and css. So I had to go for the try and error approach.

The best thing about Jekyll is that when you want to install it, you have to right a single line of command in the command prompt console and you are done. It will go and look for dependency Ruby packages (called gems), install them automatically, and when the process is done, you are ready to start a web site. The only other thing remaining would be to run another command on the console each time you want to run the server as local or you want to build the website. But when you run the server, checking the changes on template is blazing fast, you just change the code, go back to browser and refresh and it would take less than 4 seconds to update the page to your changes.

Building the website is another story, the only down side with Jekyll in particular is that it won't track your changes on the hard drive, meaning that if you have 100 posts, and 60 of them are 4 years old and never opened or changed by you any more, each time you build your site, it will regenerate those pages as well. That is why some other solutions were born, but my personal experience with them were not as smooth and productive as jekyll. Another wierd thing about current version of jekyll (3.0.1) is that when you are running the server locally, you will see the correct css and styles being used, but when you build the site and check the generated html files, you would think the builder had dropped all the style sheets, but when you upload the content into your repository of your choice (gitbub pages or bit bucket) and check the index link, you would see that the css and styles are being used, so I am not sure what is the deal there, as long as it works on the website, that would be good enough for me.