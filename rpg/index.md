---
layout: page
title: RPG Core Road Map
overview5: true
---
This page is more like a road map for myself and those who are interested in the project. I will come to this page and add or tweak content and time passes. If I find that I forgot about a system, or I found a new system that could be injected into project, I will add it here. At each system level, I will break the system into the smaller sections and in front of each one of them, you will see what I have done about it and what parts remained untouched.


<div class="note">
  <h5>Item System</h5>
  <p>Item system is responsible of keeping every possible item in game in a database and give you the ability to access them in game.</p>
</div>
1. Item Database << Finished R&D, Implemented iteration 1
2. Database Editor << Finished R&D, Implemented iteration 1

<div class="note">
  <h5>Node Editor</h5>
  <p>The base for a node editor system. We will use this system later on in many places, like behavior tree and dialogue editor.</p>
</div>
1. Graph System << Finished R&D, Implemented iteration 5
2. Custom Editor << Finished R&D, Implemented iteration 5
3. Base Node Types << Doing R&D

<div class="note">
  <h5>Base Character</h5>
  <p>Everything related to characters in game, player, enemy or npc characters will share this base system.</p>
</div>
1. Base Character Class
2. Character Stats

<div class="note">
  <h5>Character Motor</h5>
  <p>The system who is responsible of moving characters aroung in world.</p>
</div>
1. Player Motor
2. NPC Motor
3. Mecanim

<div class="note">
  <h5>AI</h5>
  <p>Would you have a game without artificial intelligence ?</p>
</div>
1. Base Behavior Tree
2. Behavior Tree Nodes
3. Agent Class
4. Action Classes
5. FSM << Maybe not needed
6. Fighting Mechanics
7. Path Finding / Navigation

<div class="note">
  <h5>Dialogue System</h5>
  <p>Will use the node editor system combined with quest system, to make it possible to communicate with players.</p>
</div>
1. Database
2. Editor
3. Game GUI

<div class="note">
  <h5>Quest System</h5>
  <p>This system is responsible of handling any sort of advancement for player, being it exp, item powerups or anything else that boosts player.</p>
</div>
1. Database
2. Editor
3. In Game Handling

<div class="note">
  <h5>Inventory System</h5>
  <p>No matter what type of game you are developing, your players will always carry around stuff.</p>
</div>
1. Database
2. Editor
3. Game GUI

<div class="note">
  <h5>Save Load</h5>
  <p>The only games that really don't need this is mobile archade games. Any other game, and you woul need a way to save the game and let the player load it later.</p>
</div>
1. Serialization
2. Deserialization

<div class="note">
  <h5>Skill System</h5>
  <p>This system is responsible of giving players the option to higher up their skills in game, at the same time, skills will be used in game mechanics.</p>
</div>

<div class="note">
  <h5>Ability System</h5>
  <p>Ability system is so much similar with skill system that many developers combine them, but I want to keep them separated.</p>
</div>